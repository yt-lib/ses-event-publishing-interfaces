import type { DeliveryEvent } from './DeliveryEvent'

export type DeliveryNotification = Omit<DeliveryEvent, 'eventType'> & {
	notificationType: DeliveryEvent['eventType']
}
