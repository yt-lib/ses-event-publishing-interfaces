import { BounceEvent } from './BounceEvent'
import { ClickEvent } from './ClickEvent'
import { ComplaintEvent } from './ComplaintEvent'
import { DeliveryDelayEvent } from './DeliveryDelayEvent'
import { DeliveryEvent } from './DeliveryEvent'
import { OpenEvent } from './OpenEvent'
import { RejectEvent } from './RejectEvent'
import { RenderingFailureEvent } from './RenderingFailureEvent'
import { SendEvent } from './SendEvent'

export type Event =
	| BounceEvent
	| ClickEvent
	| ComplaintEvent
	| DeliveryDelayEvent
	| DeliveryEvent
	| OpenEvent
	| RejectEvent
	| RenderingFailureEvent
	| SendEvent
