import type { ComplaintEvent } from './ComplaintEvent'

export type ComplaintNotification = Omit<
	ComplaintEvent,
	'eventType' | 'complaint'
> & {
	notificationType: ComplaintEvent['eventType']
	complaint: Pick<
		ComplaintEvent['complaint'],
		'complainedRecipients' | 'timestamp' | 'feedbackId'
	>
}
