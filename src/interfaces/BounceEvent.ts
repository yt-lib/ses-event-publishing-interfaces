import { Mail } from './Mail'

export type BounceEvent = {
	eventType: 'Bounce'
	bounce: Bounce
	mail: Mail
}

/**
 * | bounceType | bounceSubType | Description
 * | :-- | :-- | :--
 * | Undetermined | Undetermined | Amazon SES was unable to determine a specific bounce reason.
 * | Permanent | General | Amazon SES received a general hard bounce. If you receive this type of bounce, you should remove the recipient's email address from your mailing list.
 * | Permanent | NoEmail | Amazon SES received a permanent hard bounce because the target email address does not exist. If you receive this type of bounce, you should remove the recipient's email address from your mailing list.
 * | Permanent | Suppressed | Amazon SES has suppressed sending to this address because it has a recent history of bouncing as an invalid address. For information about how to remove an address from the suppression list, see Using the Amazon SES global suppression list.
 * | Permanent | OnAccountSuppressionList | Amazon SES has suppressed sending to this address because it is on the account-level suppression list.
 * | Transient | General | Amazon SES received a general bounce. You may be able to successfully send to this recipient in the future.
 * | Transient | MailboxFull | Amazon SES received a mailbox full bounce. You may be able to successfully send to this recipient in the future.
 * | Transient | MessageTooLarge | Amazon SES received a message too large bounce. You may be able to successfully send to this recipient if you reduce the size of the message.
 * | Transient | ContentRejected | Amazon SES received a content rejected bounce. You may be able to successfully send to this recipient if you change the content of the message.
 * | Transient | AttachmentRejected | Amazon SES received an attachment rejected bounce. You may be able to successfully send to this recipient if you remove or change the attachment.
 */
export type Bounce = {
	bounceType: 'Undetermined' | 'Permanent' | 'Transient'
	bounceSubType:
		| 'Undetermined'
		| 'General'
		| 'NoEmail'
		| 'Suppressed'
		| 'OnAccountSuppressionList'
		| 'General'
		| 'MailboxFull'
		| 'MessageTooLarge'
		| 'ContentRejected'
		| 'AttachmentRejected'
	bouncedRecipients: BouncedRecipient[]
	timestamp: string
	/** A unique ID for the complaint. */
	feedbackId: string
	reportingMTA?: string
	remoteMtaIp?: string
}

export type BouncedRecipient = {
	/** The email address of the recipient. If a DSN is available, this is the value of the `Final-Recipient` field from the DSN. */
	emailAddress: string
	/** The value of the Action field from the DSN. This indicates the action performed by the reporting MTA as a result of its attempt to deliver the message to this recipient. */
	action?: string
	/** The value of the Status field from the DSN. This is the per-recipient transport-independent status code that indicates the delivery status of the message. */
	status?: string
	/** The status code issued by the reporting MTA. This is the value of the `Diagnostic-Code` field from the DSN. This field may be absent in the DSN (and therefore also absent in the JSON). */
	diagnosticCode?: string
}

export const exampleBounceEvent: BounceEvent = {
	eventType: 'Bounce',
	bounce: {
		bounceType: 'Permanent',
		bounceSubType: 'General',
		bouncedRecipients: [
			{
				emailAddress: 'recipient@example.com',
				action: 'failed',
				status: '5.1.1',
				diagnosticCode: 'smtp; 550 5.1.1 user unknown',
			},
		],
		timestamp: '2017-08-05T00:41:02.669Z',
		feedbackId: '01000157c44f053b-61b59c11-9236-11e6-8f96-7be8aexample-000000',
		reportingMTA: 'dsn; mta.example.com',
	},
	mail: {
		timestamp: '2017-08-05T00:40:02.012Z',
		source: 'Sender Name <sender@example.com>',
		sourceArn: 'arn:aws:ses:us-east-1:123456789012:identity/sender@example.com',
		sendingAccountId: '123456789012',
		messageId: 'EXAMPLE7c191be45-e9aedb9a-02f9-4d12-a87d-dd0099a07f8a-000000',
		destination: ['recipient@example.com'],
		headersTruncated: false,
		headers: [
			{
				name: 'From',
				value: 'Sender Name <sender@example.com>',
			},
			{
				name: 'To',
				value: 'recipient@example.com',
			},
			{
				name: 'Subject',
				value: 'Message sent from Amazon SES',
			},
			{
				name: 'MIME-Version',
				value: '1.0',
			},
			{
				name: 'Content-Type',
				value:
					'multipart/alternative; boundary="----=_Part_7307378_1629847660.1516840721503"',
			},
		],
		commonHeaders: {
			from: ['Sender Name <sender@example.com>'],
			to: ['recipient@example.com'],
			messageId: 'EXAMPLE7c191be45-e9aedb9a-02f9-4d12-a87d-dd0099a07f8a-000000',
			subject: 'Message sent from Amazon SES',
		},
		tags: {
			'ses:configuration-set': ['ConfigSet'],
			'ses:source-ip': ['192.0.2.0'],
			'ses:from-domain': ['example.com'],
			'ses:caller-identity': ['ses_user'],
		},
	},
}
