import { Mail } from './Mail'

/**
 * The type of delay. Possible values are:
 * - InternalFailure – An internal Amazon SES issue caused the message to be delayed.
 * - General – A generic failure occurred during the SMTP conversation.
 * - MailboxFull – The recipient's mailbox is full and is unable to receive additional messages.
 * - SpamDetected – The recipient's mail server has detected a large amount of unsolicited email from your account.
 * - RecipientServerError – A temporary issue with the recipient's email server is preventing the delivery of the message.
 * - IPFailure – The IP address that's sending the message is being blocked or throttled by the recipient's email provider.
 * - TransientCommunicationGeneral – There was a temporary communication failure during the SMTP conversation with the recipient's email provider.
 * - BYOIPHostNameLookupUnavailable – Amazon SES was unable to look up the DNS hostname for your IP addresses. This type of delay only occurs when you use Bring Your Own IP.
 * - Undetermined – Amazon SES wasn't able to determine the reason for the delivery delay.
 */
export type DelayType =
	/** An internal Amazon SES issue caused the message to be delayed. */
	| 'InternalFailure'
	/** A generic failure occurred during the SMTP conversation. */
	| 'General'
	/** The recipient's mailbox is full and is unable to receive additional messages. */
	| 'MailboxFull'
	/** The recipient's mail server has detected a large amount of unsolicited email from your account. */
	| 'SpamDetected'
	/** A temporary issue with the recipient's email server is preventing the delivery of the message. */
	| 'RecipientServerError'
	/** The IP address that's sending the message is being blocked or throttled by the recipient's email provider. */
	| 'IPFailure'
	/** There was a temporary communication failure during the SMTP conversation with the recipient's email provider. */
	| 'TransientCommunicationGeneral'
	/** Amazon SES was unable to look up the DNS hostname for your IP addresses. This type of delay only occurs when you use Bring Your Own IP. */
	| 'BYOIPHostNameLookupUnavailable'
	/** Amazon SES wasn't able to determine the reason for the delivery delay. */
	| 'Undetermined'
	// what?
	| string

export type DeliveryDelayEvent = {
	eventType: 'DeliveryDelay'
	mail: Mail
	deliveryDelay: {
		timestamp: string
		delayType: DelayType
		expirationTime: string
		delayedRecipients: {
			emailAddress: string
			/** The SMTP status code associated with the delivery delay. */
			status: string
			diagnosticCode: string
		}[]
	}
}

export const exampleDeliveryDelayEvent: DeliveryDelayEvent = {
	eventType: 'DeliveryDelay',
	mail: {
		timestamp: '2020-06-16T00:15:40.641Z',
		source: 'sender@example.com',
		sourceArn: 'arn:aws:ses:us-east-1:123456789012:identity/sender@example.com',
		sendingAccountId: '123456789012',
		messageId: 'EXAMPLE7c191be45-e9aedb9a-02f9-4d12-a87d-dd0099a07f8a-000000',
		destination: ['recipient@example.com'],
		headersTruncated: false,
		tags: {
			'ses:configuration-set': ['ConfigSet'],
		},
	},
	deliveryDelay: {
		timestamp: '2020-06-16T00:25:40.095Z',
		delayType: 'TransientCommunicationFailure',
		expirationTime: '2020-06-16T00:25:40.914Z',
		delayedRecipients: [
			{
				emailAddress: 'recipient@example.com',
				status: '4.4.1',
				diagnosticCode: 'smtp; 421 4.4.1 Unable to connect to remote host',
			},
		],
	},
}
