export type Mail = {
	commonHeaders?: Record<string, string | string[]>
	destination: string[]
	headers?: { name: string; value: string }[]
	/** A string that specifies whether the headers are truncated in the notification, which occurs if the headers are larger than 10 KB. Possible values are `true` and `false`. */
	headersTruncated?: boolean
	messageId: string
	sendingAccountId: string
	source?: string
	sourceIp?: string
	sourceArn?: string
	tags?: Record<string, string[]>
	/** The date and time, in ISO8601 format (`YYYY-MM-DDThh:mm:ss.sZ`), when the message was sent. */
	timestamp: string
}
