import { Mail } from './Mail'

export type ComplaintEvent = {
	eventType: 'Complaint'
	complaint: {
		complainedRecipients: {
			emailAddress: string
		}[]
		timestamp: string
		feedbackId: string
		userAgent: string
		complaintFeedbackType: ComplaintFeedbackType
		arrivalDate: string
	}
	mail: Mail
}

/**
 * You may see the following complaint types in the complaintFeedbackType field as assigned by the reporting ISP, according to the [Internet Assigned Numbers Authority website](http://www.iana.org/assignments/marf-parameters/marf-parameters.xml#marf-parameters-2):
 * - `abuse` Indicates unsolicited email or some other kind of email abuse.
 * - `auth-failure` Email authentication failure report.
 * - `fraud` Indicates some kind of fraud or phishing activity.
 * - `not-spam` Indicates that the entity providing the report does not consider the message to be spam. This may be used to correct a message that was incorrectly tagged or categorized as spam.
 * - `other` Indicates any other feedback that does not fit into other registered types.
 * - `virus` Reports that a virus is found in the originating message.
 */
export type ComplaintFeedbackType =
	| 'abuse'
	| 'auth-failure'
	| 'fraud'
	| 'not-spam'
	| 'other'
	| 'virus'
	| string

export const exampleComplaintEvent: ComplaintEvent = {
	eventType: 'Complaint',
	complaint: {
		complainedRecipients: [
			{
				emailAddress: 'recipient@example.com',
			},
		],
		timestamp: '2017-08-05T00:41:02.669Z',
		feedbackId: '01000157c44f053b-61b59c11-9236-11e6-8f96-7be8aexample-000000',
		userAgent:
			'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
		complaintFeedbackType: 'abuse',
		arrivalDate: '2017-08-05T00:41:02.669Z',
	},
	mail: {
		timestamp: '2017-08-05T00:40:01.123Z',
		source: 'Sender Name <sender@example.com>',
		sourceArn: 'arn:aws:ses:us-east-1:123456789012:identity/sender@example.com',
		sendingAccountId: '123456789012',
		messageId: 'EXAMPLE7c191be45-e9aedb9a-02f9-4d12-a87d-dd0099a07f8a-000000',
		destination: ['recipient@example.com'],
		headersTruncated: false,
		headers: [
			{
				name: 'From',
				value: 'Sender Name <sender@example.com>',
			},
			{
				name: 'To',
				value: 'recipient@example.com',
			},
			{
				name: 'Subject',
				value: 'Message sent from Amazon SES',
			},
			{
				name: 'MIME-Version',
				value: '1.0',
			},
			{
				name: 'Content-Type',
				value:
					'multipart/alternative; boundary="----=_Part_7298998_679725522.1516840859643"',
			},
		],
		commonHeaders: {
			from: ['Sender Name <sender@example.com>'],
			to: ['recipient@example.com'],
			messageId: 'EXAMPLE7c191be45-e9aedb9a-02f9-4d12-a87d-dd0099a07f8a-000000',
			subject: 'Message sent from Amazon SES',
		},
		tags: {
			'ses:configuration-set': ['ConfigSet'],
			'ses:source-ip': ['192.0.2.0'],
			'ses:from-domain': ['example.com'],
			'ses:caller-identity': ['ses_user'],
		},
	},
}
