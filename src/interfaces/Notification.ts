import type { BounceNotification } from './BounceNotification'
import type { ComplaintNotification } from './ComplaintNotification'
import type { DeliveryNotification } from './DeliveryNotification'

export type Notification =
	| BounceNotification
	| ComplaintNotification
	| DeliveryNotification
