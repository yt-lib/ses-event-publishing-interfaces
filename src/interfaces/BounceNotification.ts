import type { BounceEvent } from './BounceEvent'

export type BounceNotification = Omit<BounceEvent, 'eventType'> & {
	notificationType: BounceEvent['eventType']
}
