import { join } from 'path'
import { remove } from 'fs-extra'

module.parent ||
	remove(join(__dirname, '..', 'dist')).catch(x => {
		console.error(x)
		process.exit(1)
	})
