下記の AWS のドキュメントをもとにした型情報

[Amazon SES イベント発行を使用して E メール送信をモニタリングする](https://docs.aws.amazon.com/ja_jp/ses/latest/DeveloperGuide/monitor-using-event-publishing.html)  
[Amazon SES イベントデータの使用](https://docs.aws.amazon.com/ja_jp/ses/latest/DeveloperGuide/working-with-event-data.html)  

